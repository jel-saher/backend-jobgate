const Offre = require("../Models/Offre");
const Entreprise = require("../Models/User");

const createOffre = async (req, res) => {
  try {
    const newOffre = new Offre(req.body);
    //ancien methode
    //const offre = await Offre.create(newOffre);
    //new version
    const offre = await newOffre.save();
    await Entreprise.findByIdAndUpdate(req.body.entreprise, {
      $push: { offres: offre },
    });
    res.status(201).json({ meassge: "Offre created", data: offre });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

const getAllOffre = async (req, res) => {
  try {
    const list = await Offre.find({})
      .populate({
        path: "entreprise",
        populate: { path: "place" },
      })
      .populate({
        path: "entreprise",
        populate: { path: "specialites" },
      });
    res.status(200).json({ message: "Liste des Offres", data: list });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

const getByIdOffre = async (req, res) => {
  try {
    const list = await Offre.findById({ _id: req.params.id })
      .populate({
        path: "entreprise",
        populate: { path: "place" },
      })
      .populate({
        path: "entreprise",
        populate: { path: "specialites" },
      });
    res.status(200).json({ message: "Offre : ", data: list });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

const getByNameOffre = async (req, res) => {
  try {
    const list = await Offre.findOne({ name: req.query.name });
    res.status(200).json({ message: "Offre : ", data: list });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

const updateOffre = async (req, res) => {
  try {
    await Offre.updateOne({ _id: req.params.id }, req.body);
    res.status(200).json({ message: "Offre Updated" });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

const deleteOffre = async (req, res) => {
  try {
    const of = await Offre.findById({ _id: req.params.id });
    await Offre.deleteOne({ _id: req.params.id });
    await Entreprise.findByIdAndUpdate(of.entreprise, {
      $pull: { offres: of._id },
    });
    res.status(200).json({ message: "Offre Deleted" });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

module.exports = {
  createOffre,
  getAllOffre,
  getByIdOffre,
  getByNameOffre,
  updateOffre,
  deleteOffre,
};

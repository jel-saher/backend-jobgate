const Category = require("../Models/Category");

const createCategory = async (req, res) => {
  try {
    const newCategory = new Category(req.body);
    //ancien methode
    //const category = await Category.create(newCategory);
    //new version
    const category = await newCategory.save();
    res.status(201).json({ meassge: "Category created", data: category });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

const getAllCategory = async (req, res) => {
  try {
    const liste = await Category.find({});
    res.status(200).json({ message: "Liste des category", data: liste });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

const getByIdCategory = async (req, res) => {
  try {
    const liste = await Category.findById({ _id: req.params.id });
    res.status(200).json({ message: "Category : ", data: liste });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

const getByNameCategory = async (req, res) => {
  try {
    const liste = await Category.findOne({ name: req.query.name });
    res.status(200).json({ message: "Category : ", data: liste });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

const updateCategory = async (req, res) => {
  try {
    await Category.updateOne({ _id: req.params.id }, req.body);
    res.status(200).json({ message: "Category Updated" });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

const deleteCategory = async (req, res) => {
  try {
    await Category.deleteOne({ _id: req.params.id });
    res.status(200).json({ message: "Category Deleted" });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

module.exports = {
  createCategory,
  getAllCategory,
  getByIdCategory,
  updateCategory,
  deleteCategory,
  getByNameCategory,
};

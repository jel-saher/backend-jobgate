const Skill = require("../Models/Skill");

const createSkill = async (req, res) => {
  try {
    const newSkill = new Skill(req.body);
    //ancien methode
    //const skill = await Skill.create(newSkill);
    //new version
    const skill = await newSkill.save();
    res.status(201).json({ meassge: "Skill created", data: skill });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

const getAllSkill = async (req, res) => {
  try {
    const list = await Skill.find({});
    res.status(200).json({ message: "Liste des Skills", data: list });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

const getByIdSkill = async (req, res) => {
  try {
    const list = await Skill.findById({ _id: req.params.id });
    res.status(200).json({ message: "Skill : ", data: list });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

const getByNameSkill = async (req, res) => {
  try {
    const list = await Skill.findOne({ name: req.query.name });
    res.status(200).json({ message: "Skill : ", data: list });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

const updateSkill = async (req, res) => {
  try {
    await Skill.updateOne({ _id: req.params.id }, req.body);
    res.status(200).json({ message: "Skill Updated" });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

const deleteSkill = async (req, res) => {
  try {
    await Skill.deleteOne({ _id: req.params.id });
    res.status(200).json({ message: "Skill Deleted" });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

module.exports = {
  createSkill,
  getAllSkill,
  getByIdSkill,
  getByNameSkill,
  updateSkill,
  deleteSkill,
};

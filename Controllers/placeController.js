const Place = require("../Models/Place");
const Entreprise = require("../Models/User");

const createPlace = async (req, res) => {
  try {
    const newPlace = new Place(req.body);
    //ancien methode
    //const place = await Place.create(newOffre);
    //new version
    const place = await newPlace.save();
    res.status(201).json({ meassge: "Place created", data: place });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

const getAllPlace = async (req, res) => {
  try {
    const list = await Place.find({}).populate("entreprises");
    res.status(200).json({ message: "Liste des Place", data: list });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

const getByIdPlace = async (req, res) => {
  try {
    const list = await Place.findById({ _id: req.params.id });
    res.status(200).json({ message: "Place : ", data: list });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

const getByNamePlace = async (req, res) => {
  try {
    const list = await Place.findOne({ name: req.query.name });
    res.status(200).json({ message: "Place : ", data: list });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

const updatePlace = async (req, res) => {
  try {
    await Place.updateOne({ _id: req.params.id }, req.body);
    res.status(200).json({ message: "Place Updated" });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

const deletePlace = async (req, res) => {
  try {
    const entre = await Entreprise.findOne({ palce: req.params.id });
    await Entreprise.findByIdAndUpdate(entre.id, {
      
    });
    await Place.deleteOne({ _id: req.params.id });
    res.status(200).json({ message: "Place Deleted" });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

module.exports = {
  createPlace,
  getAllPlace,
  getByIdPlace,
  getByNamePlace,
  updatePlace,
  deletePlace,
};

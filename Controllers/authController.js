const User = require("../Models/User");
const Speciality = require("../Models/Speciality");
const Skill = require("../Models/Skill");
const Place = require("../Models/Place");
const Offre = require("../Models/Offre");

const bcrypt = require("bcrypt");
const { randomBytes } = require("crypto");
const { join } = require("path");
const nodemailer = require("nodemailer");
const jwt = require("jsonwebtoken");

const DOMAIN = process.env.APP_DOMAIN;
const SECRET = process.env.APP_SECRET;
var refreshtokens = [];

const transporteur = nodemailer.createTransport({
  service: "gmail",
  auth: { user: process.env.APP_USER, pass: process.env.APP_PASS },
});

const registerAdmin = async (req, res) => {
  try {
    const password = bcrypt.hashSync(req.body.password, 10);
    const newAdmin = new User({
      firstname: req.body.firstname,
      email: req.body.email,
      password: password,
      role: "admin",
      confirmed: true,
    });
    const admin = await User.create(newAdmin);
    res.status(201).json({
      message: "Your account is created",
      data: admin,
    });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

const register = async (req, res) => {
  try {
    req.body["picture"] = req.file.filename;
    const password = bcrypt.hashSync(req.body.password, 10);
    const newUser = new User({
      ...req.body,
      password,
    });
    const user = await User.create(newUser);
    await user.save();
    if (!req.body.specialites) {
      await res.status(200).json({
        message: "Your account is created",
      });
    } else {
      /* for (var i = 0; i < req.body.specialites.length; i++) {
       
      } */

      await Speciality.findByIdAndUpdate(req.body.specialites, {
        $push: { entreprises: user },
      });

      await Place.findByIdAndUpdate(req.body.place, {
        $push: { entreprises: user },
      });

      /*  await Skill.findOneAndUpdate(req.body.skill, {
      $push: { entreprises: user },
    });
 */
      await res.status(201).json({
        message: "Your account is created",
        data: user,
      });
    }
  } catch (error) {
    res.status(501).json({ message: error.message });
  }
};

const login = async (req, res) => {
  try {
    const user = await User.findOne({ email: req.body.email });
    if (!user) {
      return res.status(500).json({ message: "Invalid Email" });
    }
    const valid = await bcrypt.compareSync(req.body.password, user.password);
    if (!valid) {
      return res.status(501).json({ message: "Password False" });
    }
    if (user.confirmed === false) {
      res.status(502).json({ message: "Your Account Is Not Confirmed" });
    } else {
      const token = jwt.sign({ id: user._id, user: user }, SECRET, {
        expiresIn: "24h",
      });
      const refreshtoken = jwt.sign({ id: user._id, user: user }, SECRET, {
        expiresIn: "24h",
      });
      refreshtokens[refreshtoken] = user._id;

      const result = {
        Email: user.email,
        User: user,
        Token: token,
        ExpiresIn: 1,
        refreshtoken: refreshtoken,
      };
      res.status(200).json({ message: user.name + " is logged", ...result });
    }
  } catch (error) {
    res.status(404).json({
      message: error.message,
      status: 404,
    });
  }
};

const refreshToken = async (req, res) => {
  try {
    const refreshToken = req.body.refreshtoken;
    if (refreshToken in refreshtokens) {
      const token = jwt.sign({ id: req.user._id }, SECRET, { expiresIn: "7h" });
      const refreshToken = jwt.sign({ id: req.user._id }, SECRET, {
        expiresIn: "24h",
      });
      refreshtokens[refreshToken] = req.user._id;
      res.status(200).json({ accessToken: token, refreshToken: refreshToken });
    }
  } catch (error) {
    res.status(404).json({
      message: error.message,
      status: 404,
    });
  }
};

const profil = async (req, res) => {
  try {
    const user = await req.user;
    res.status(200).json({ message: "Profil identify", data: user });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

const confirmedUser = async (req, res) => {
  try {
    const user = await User.findById({ _id: req.params.iduser });
    if (!user) {
      res.status(500).json({ message: "User indifiened" });
    }
    user.confirmed = true;
    await user.save();
    res.status(200).json({ message: "User Confirmed", data: user });
    transporteur.sendMail(
      {
        to: user.email,
        subject: "Welcome " + user.firstname,
        text: "Bonjour ",
        html: `<!DOCTYPE html>
            <html lang="en">
            <head>
                <meta charset="UTF-8">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <title>Welcome Email</title>
            </head>
            <body>
                <h2>Hello ${user.firstname} your account is verified</h2>
                <p>We are glad to have you on bord at ${user.email}</p>
                <a href="">Thank you for joining our platforme</a>
            </body>
            </html>`,
      },

      function (err, info) {
        if (err) {
          console.log("error : " + err.message);
        } else {
          console.log("email send : " + info.res);
        }
      }
    );
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

const forgetpassword = async (req, res) => {
  try {
    const user = await User.findOne({ email: req.body.email });
    console.log(user);
    if (!user) {
      return res.status(500).json({ message: "Your email does not exist" });
    }
    const token = jwt.sign({ id: user._id }, SECRET, {
      expiresIn: "2h",
    });
    user.resetpassword = token;
    user.save();
    console.log(token);
    transporteur.sendMail(
      {
        to: user.email,
        subject: "Forget Password",
        text: "Bonjour MR",
        html: `<!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title>Welcome Email</title>
        </head>
        <body>
            <h2>
                Hello ${user.firstname}
            </h2>
            <p>We are glad to have you on bord at ${user.email}</p>
            <a href="http://localhost:4200/resetpassword/${token}">Reset Password</a>
        </body>
        </html>`,
      },
      //fonction pour afficher msg lors de la creation de client lors de lenvoi du email
      function (err, info) {
        if (err) {
          console.log("error : " + err.message);
        } else {
          console.log("email send : " + info.response);
        }
      }
    );
    return res.status(200).json({ message: "email send" });
  } catch (error) {}
};

const resetpassword = async (req, res) => {
  try {
    const resetpassword = req.body.resetpassword;
    jwt.verify(resetpassword, SECRET, async (err) => {
      if (err) {
        return res.json({ error: "incorrect token or it is expired" });
      }

      const user = await User.findOne({ resetpassword: resetpassword });
      console.log("user : ", user);
      const password = bcrypt.hashSync(req.body.newpassword, 10);
      user.password = password;
      user.save();
      res.status(200).json({ message: "password modified", data: user });
    });
  } catch (error) {
    res.status(500).json({ message: error.meessage });
  }
};

const changePassword = async (req, res) => {
  try {
    const user = await req.user;
    const comp = bcrypt.compareSync(req.body.oldpassword, user.password);
    if (!comp) {
      res.status(500).json({ message: "old password is false" });
    } else {
      user.password = bcrypt.hashSync(req.body.newpassword, 10);
      user.save();
      res.status(200).json({ message: "password changed" });
    }
  } catch (error) {
    res.status(500).json({ message: error.meessage });
  }
};

const getAllUser = async (req, res) => {
  try {
    const list = await User.find({}).populate("place").populate("specialites");
    res.status(200).json({ message: "Liste des Users", data: list });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

const getByIdUser = async (req, res) => {
  try {
    byiduser = await User.findById({ _id: req.params.id })
      .populate("place")
      .populate("specialites");
    res.status(200).json({ message: "User :", data: byiduser });
  } catch (error) {}
};

const deleteEntreprise = async (req, res) => {
  try {
    await Speciality.findOneAndUpdate(
      { entreprises: req.params.id },
      {
        $pull: { entreprises: req.params.id },
      }
    );
    for (let i = 0; i < Offre.length; i++) {
      const offre = await Offre.findOne({ entrprise: req.params.id });
      await Offre.deleteOne(offre);
    }
    await User.deleteOne({ _id: req.params.id });
    res.status(200).json({ message: "Entreprise deleted" });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

const confirmedOffre = async (req, res) => {
  try {
    const offre = await Offre.findById({ _id: req.params.id });
    console.log("offre :", offre);
    const user = await User.findOne({ offres: offre });
    console.log("Entreprise :", user);
    if (!user) {
      res.status(500).json({ message: "User indifiened" });
    }
    offre.confirmed = true;
    await offre.save();
    res.status(200).json({ message: "Offre Confirmed", data: user });
    transporteur.sendMail(
      {
        to: user.email,
        subject: "Welcome " + user.name,
        text: "Bonjour ",
        html: `<!DOCTYPE html>
            <html lang="en">
            <head>
                <meta charset="UTF-8">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <title>Welcome Email</title>
            </head>
            <body>
                <h2>Hello ${user.firstname} your offre is confirmed</h2>
                <p>We are glad to have you on bord at ${user.email}</p>
                <a href="">Thank you for joining our platforme</a>
            </body>
            </html>`,
      },

      function (err, info) {
        if (err) {
          console.log("error : " + err.message);
        } else {
          console.log("email send : " + info.res);
        }
      }
    );
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};
module.exports = {
  registerAdmin,
  register,
  login,
  profil,
  confirmedUser,
  forgetpassword,
  resetpassword,
  refreshToken,
  changePassword,
  getAllUser,
  getByIdUser,
  deleteEntreprise,
  confirmedOffre,
};

const Speciality = require("../Models/Speciality");

const createSpeciality = async (req, res) => {
  try {
    const newSpeciality = new Speciality(req.body);
    //ancien methode
    //const category = await Category.create(newCategory);
    //new version
    const speciality = await newSpeciality.save();
    res.status(201).json({ meassge: "Speciality created", data: speciality });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

const getAllSpeciality = async (req, res) => {
  try {
    const liste = await Speciality.find({}).populate("entreprises");
    res.status(200).json({ message: "Liste des Speciality", data: liste });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

const getByIdSpeciality = async (req, res) => {
  try {
    const liste = await Speciality.findById({ _id: req.params.id });
    res.status(200).json({ message: "Speciality : ", data: liste });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

const getByNameSpeciality = async (req, res) => {
  try {
    const liste = await Speciality.findOne({ name: req.query.name });
    res.status(200).json({ message: "Speciality : ", data: liste });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

const updateSpeciality = async (req, res) => {
  try {
    await Speciality.updateOne({ _id: req.params.id }, req.body);
    res.status(200).json({ message: "Speciality Updated" });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

const deleteSpeciality = async (req, res) => {
  try {
    await Speciality.deleteOne({ _id: req.params.id });
    res.status(200).json({ message: "Speciality Deleted" });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

module.exports = {
  createSpeciality,
  getAllSpeciality,
  getByIdSpeciality,
  getByNameSpeciality,
  updateSpeciality,
  deleteSpeciality,
};

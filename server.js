const express = require("express");
const cors = require("cors");
require("dotenv").config(); //pour utiliser .env
const { success, error } = require("consola");

const db = require("./Config/db");
const PORT = process.env.APP_PORT;
const DOMAIN = process.env.APP_DOMAIN;
const passport = require("passport");

const registerUser = require("./Routes/authRoute");
const category = require("./Routes/categoryRoute");
const speciality = require("./Routes/specialityRoute");
const skill = require("./Routes/skillRoute");
const offre = require("./Routes/offreRoute");
const place = require("./Routes/placeRoute");
const candidature = require("./Routes/candidatureRoute");
const message = require("./Routes/messageRoute");

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cors());
app.use(passport.initialize());

app.use("/", registerUser);
app.use("/category", category);
app.use("/speciality", speciality);
app.use("/skill", skill);
app.use("/offre", offre);
app.use("/place", place);
app.use("/candidature", candidature);
app.use("/message", message);

/* affichage image */
app.get("/getfile/:image", function (req, res) {
  res.sendFile(__dirname + "/storages/" + req.params.image);
});

app.listen(PORT, async () => {
  try {
    success({
      message:
        `Server started on PORT : ${PORT}` + `URL : http://localhost:${PORT}`,
      badge: true,
    });
  } catch (err) {
    error({
      message: "Error with server" + err.message,
      badge: true,
    });
  }
});

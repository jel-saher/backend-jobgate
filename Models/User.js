const mongoose = require("mongoose");

const userSchema = new mongoose.Schema(
  {
    email: {
      type: String,
      required: true,
      unique: true,
    },

    password: {
      type: String,
      required: true,
    },

    name: {
      type: String,
      required: false,
    },

    firstname: {
      type: String,
      required: false,
    },

    lastname: {
      type: String,
      required: false,
    },

    picture: {
      type: String,
      required: false,
    },

    mobile: {
      type: String,
      required: false,
    },

    website: {
      type: String,
      required: false,
    },

    description: {
      type: String,
      required: false,
    },
    poste: {
      type: String,
      required: false,
    },

    confirmed: {
      type: Boolean,
      default: false,
    },

    resetpassword: {
      type: String,
      required: false,
    },

    role: {
      type: String,
      enum: ["candidat", "admin", "entreprise"],
    },
    /*  skills: [
      {
        type: mongoose.Types.ObjectId,
        ref: "Skill",
      },
    ], */

    specialites: {
      type: mongoose.Types.ObjectId,
      ref: "Speciality",
    },

    messages: [
      {
        type: mongoose.Types.ObjectId,
        ref: "Message",
      },
    ],

    offres: [
      {
        type: mongoose.Types.ObjectId,
        ref: "Offre",
      },
    ],

    candidatures: [
      {
        type: mongoose.Types.ObjectId,
        ref: "Candidature",
      },
    ],

    place: {
      type: mongoose.Types.ObjectId,
      ref: "Place",
    },

    /*  specialite: {
    type: mongoose.Types.ObjectId,
    ref: "Speciality",
    required: false,
  }, */
  },

  {
    timestamps: true,
  }
);

//Export the model
module.exports = mongoose.model("User", userSchema);

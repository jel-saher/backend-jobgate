const { urlencoded } = require("express");
const mongoose = require("mongoose");

var placeSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
      unique: true,
    },

    map: {
      type: String,
      required: false,
      unique: true,
    },

    entreprises: [
      {
        type: mongoose.Types.ObjectId,
        ref: "User",
      },
    ],
  },
  { timestamps: true }
);

//Export the model
module.exports = mongoose.model("Place", placeSchema);

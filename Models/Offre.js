const mongoose = require("mongoose");

var offreSchema = new mongoose.Schema(
  {
    titre: {
      type: String,
      required: true,
    },
    type: {
      type: String,
      required: true,
    },

    description: {
      type: String,
    },

    entreprise: {
      type: mongoose.Types.ObjectId,
      ref: "User",
      required: false,
    },

    candidatures: [
      {
        type: mongoose.Types.ObjectId,
        ref: "Candidature",
      },
    ],

    confirmed: {
      type: Boolean,
      default: false,
    },
  },
  { timestamps: true }
);

module.exports = mongoose.model("Offre", offreSchema);

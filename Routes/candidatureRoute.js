const route = require("express").Router();
const candidaturecontroller = require("../Controllers/candidatureController");
const uploadimage = require("../Middelwares/uploadImage");
const uploadfile = require("../Middelwares/uploadFile");
const passport_admin = require("passport");
require("../Middelwares/passport_admin").passport;
route.post(
  "/createcandidature",
  uploadfile.single("cv"),
  candidaturecontroller.createcandidature
);
route.get("/getallcandidature", candidaturecontroller.getallsCandidature);
route.get("/getcandidaturebyid/:id", candidaturecontroller.getcandidaturetById);
route.get("/getcandidaturebyname", candidaturecontroller.getcandidatureByname);
route.put("/updatecandidature/:id", candidaturecontroller.Updatecandidature);
route.delete("/deletecandidature/:id", candidaturecontroller.Deletecandidature);
module.exports = route;

const route = require("express").Router();
const categoryController = require("../Controllers/categoryController");
const passport_admin = require("passport");
require("../Middelwares/passport_admin").passport;

route.post(
  "/createcategory",
  passport_admin.authenticate("jwt", { session: false }),
  categoryController.createCategory
);
route.get("/listcategory", categoryController.getAllCategory);
route.get("/byidcategory/:id", categoryController.getByIdCategory);
route.get("/bynamecategory", categoryController.getByNameCategory);
route.put(
  "/updatecategory/:id",
  passport_admin.authenticate("jwt", { session: false }),
  categoryController.updateCategory
);
route.delete(
  "/deletecategory/:id",
  passport_admin.authenticate("jwt", { session: false }),
  categoryController.deleteCategory
);

module.exports = route;

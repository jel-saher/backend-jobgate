const route = require("express").Router();
const placeController = require("../Controllers/placeController");
const passport_admin = require("passport");
require("../Middelwares/passport_admin").passport;

route.post(
  "/createplace",
  passport_admin.authenticate("jwt", { session: false }),
  placeController.createPlace
);
route.get(
  "/listplace",
  // passport_admin.authenticate("jwt", { session: false }),
  placeController.getAllPlace
);
route.get(
  "/byidplace/:id",
  // passport_admin.authenticate("jwt", { session: false }),
  placeController.getByIdPlace
);
route.get(
  "/bynameplace",
  passport_admin.authenticate("jwt", { session: false }),
  placeController.getByNamePlace
);
route.put(
  "/updateplace/:id",
  passport_admin.authenticate("jwt", { session: false }),
  placeController.updatePlace
);
route.delete(
  "/deleteplace/:id",
  passport_admin.authenticate("jwt", { session: false }),
  placeController.deletePlace
);

module.exports = route;

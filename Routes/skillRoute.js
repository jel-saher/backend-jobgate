const route = require("express").Router();
const skillController = require("../Controllers/skillController");
const passport_admin = require("passport");
require("../Middelwares/passport_admin").passport;

route.post("/createskill", skillController.createSkill);
route.get("/listskill", skillController.getAllSkill);
route.get("/byidskill/:id", skillController.getByIdSkill);
route.get("/bynamespeciality", skillController.getByNameSkill);
route.put("/updateskill/:id", skillController.updateSkill);
route.delete("/deleteskill/:id", skillController.deleteSkill);

module.exports = route;

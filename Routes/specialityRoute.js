const route = require("express").Router();
const specialityController = require("../Controllers/specialityController");
const passport_admin = require("passport");
require("../Middelwares/passport_admin").passport;

route.post(
  "/createspeciality",
  passport_admin.authenticate("jwt", { session: false }),
  specialityController.createSpeciality
);
route.get("/listspeciality", specialityController.getAllSpeciality);
route.get("/byidspeciality/:id", specialityController.getByIdSpeciality);
route.get("/bynamespeciality", specialityController.getByNameSpeciality);
route.put(
  "/updatespeciality/:id",
  passport_admin.authenticate("jwt", { session: false }),
  specialityController.updateSpeciality
);
route.delete(
  "/deletespeciality/:id",
  passport_admin.authenticate("jwt", { session: false }),
  specialityController.deleteSpeciality
);

module.exports = route;

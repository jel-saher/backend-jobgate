const route = require("express").Router();
const offreController = require("../Controllers/offreController");
const passport_admin = require("passport");
require("../Middelwares/passport_admin").passport;

route.post("/createoffre", offreController.createOffre);
route.get("/listoffre", offreController.getAllOffre);
route.get("/byidoffre/:id", offreController.getByIdOffre);
route.get("/bynameoffre", offreController.getByNameOffre);
route.put("/updateoffre/:id", offreController.updateOffre);
route.delete("/deleteoffre/:id", offreController.deleteOffre);



module.exports = route;

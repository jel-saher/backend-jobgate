const route = require("express").Router();
const authController = require("../Controllers/authController");
const uploadImage = require("../Middelwares/uploadImage");
const passport = require("passport");
require("../Middelwares/passport").passport;
const passport_admin = require("passport");
const { get } = require("express/lib/response");
require("../Middelwares/passport_admin").passport;

//route.post("/register", authController.register);
route.post("/register", uploadImage.single("image"), authController.register);
route.post("/login", authController.login);
route.get(
  "/refreshtoken",
  passport.authenticate("jwt", { session: false }),
  authController.refreshToken
);

/* route.get(
  "/profil",
  passport.authenticate("jwt", { session: false }),
  authController.profil
);  */

route.get("/userbyid/:id", authController.getByIdUser);
route.get("/getalluser");
// partie admin

route.post("/registeradmin", authController.registerAdmin);
route.get(
  "/profil",
  passport.authenticate("jwt", { session: false }),
  authController.profil
);

route.put(
  "/confirmeduser/:iduser",
  // passport_admin.authenticate("jwt", { session: false }),
  authController.confirmedUser
);

//

route.post("/forgetpassword", authController.forgetpassword);
route.post("/resetpassword", authController.resetpassword);
route.put(
  "/changepassword",
  passport.authenticate("jwt", { session: false }),
  authController.changePassword
);
route.get(
  "/getalluser",
  // passport_admin.authenticate("jwt", { session: false }),
  authController.getAllUser
);
route.delete("/deleteuser/:id", authController.deleteEntreprise);

route.put(
  "/confirmedoffre/:id",
  passport_admin.authenticate("jwt", { session: false }),
  authController.confirmedOffre
);

module.exports = route;
